from flask import Flask, request, jsonify, send_file
from confluent_kafka import Producer
import requests
import os

app = Flask(__name__)

DOWNLOAD_FOLDER = 'downloads'
app.config['DOWNLOAD_FOLDER'] = DOWNLOAD_FOLDER

if not os.path.exists(DOWNLOAD_FOLDER):
    os.makedirs(DOWNLOAD_FOLDER)

def download_csv(url, save_path):
    response = requests.get(url)
    if response.status_code == 200:
        with open(save_path, 'wb') as f:
            f.write(response.content)
        return True
    return False

def send_to_kafka(file_path):
    conf = {
        'bootstrap.servers': 'localhost:9092', 
    }
    producer = Producer(conf)
    topic = 'kafka_shopee_topic'  
    with open(file_path, 'r') as csvfile:
        csv_content = csvfile.read()
    producer.produce(topic, key='csv_key', value=csv_content)
    producer.flush()

@app.route('/api/v1/generate', methods=['POST'])
def download_csv_from_url():
    data = request.json
    csv_url = data.get('url-csv', None)
    if csv_url:
        filename = os.path.join(app.config['DOWNLOAD_FOLDER'], os.path.basename(csv_url))
        if download_csv(csv_url, filename):
            return jsonify({"message": f"CSV file downloaded successfully and saved as {filename}"})
        else:
            return jsonify({"error": "Failed to download CSV file. Invalid URL or download error."})
    else:
        return jsonify({"error": "Invalid request. Please provide a 'csv_url' in the request body."})

@app.route('/api/v1/download', methods=['GET'])
def download_csv_file():
    file_name = request.args.get('file_name')
    if file_name:
        file_path = os.path.join(app.config['DOWNLOAD_FOLDER'], file_name)
        if os.path.exists(file_path):
            return send_file(file_path, as_attachment=True, mimetype='application/octet-stream')
        else:
            return jsonify({"error": "File not found."})
    else:
        return jsonify({"error": "Invalid request. Please provide a 'file_name' query parameter."})

@app.route('/api/v1/send_mq', methods=['GET'])
def send_to_kafka_endpoint():
    file_name = request.args.get('file_name')
    if file_name:
        file_path = os.path.join(app.config['DOWNLOAD_FOLDER'], file_name)
        if os.path.exists(file_path):
            send_to_kafka(file_path)
            return jsonify({"message": "CSV file sent to Kafka successfully."})
        else:
            return jsonify({"error": "File not found."})
    else:
        return jsonify({"error": "Invalid request. Please provide a 'file_name' query parameter."})

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)
